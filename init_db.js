const config = require('./config');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

db.serialize(function() {
  db.run("\
    CREATE TABLE IF NOT EXISTS users (\
      username TEXT PRIMARY KEY,\
      password TEXT NOT NULL,\
      displayname TEXT NOT NULL,\
      timestamp TEXT NOT NULL,\
      secret TEXT NOT NULL\
    )\
  ");

  db.run("\
    CREATE TABLE IF NOT EXISTS active_tokens (\
      token TEXT PRIMARY KEY,\
      username TEXT NOT NULL,\
      expireson TEXT NOT NULL\
    )\
  ");

  db.run("INSERT OR IGNORE INTO users VALUES ('jonny', 'jonny', 'Jonny Developer', '2017-10-12 06:12:44', 'aababank')");

  console.log("username\t:password\t:timestamp");
  db.each("SELECT username, password, displayName, timestamp, secret FROM users", function(err, row) {
    console.log('users -- ' + row.username + ": " + row.password + ": " + row.displayname + ": " + row.timestamp + ": " + row.secret);
  });

  db.run("INSERT OR IGNORE INTO active_tokens VALUES ('abafaf3213', 'jonny', '2017-10-12 06:12:44')");

  console.log("id\t:username\t:token\t:expireson");
  db.each("SELECT rowid, username, token, expireson FROM active_tokens", function(err, row) {
    console.log('active_tokens -- ' + row.rowid + ": " + row.username + ": " + row.token + ": " + row.expireson);
  });

  db.all("SELECT name FROM sqlite_master WHERE type='table'", function (err, tables) {
    console.log(tables);
  });

});

db.close();
