const path = require('path');
const root_path = path.join(__dirname + '/db');
const db_path = path.join(root_path + '/database.db');

module.exports = {
  db_path,
}