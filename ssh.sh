#!/bin/bash

if [ "$1" = "0" ]; then
  ssh root@152.118.148.103 -p 20017
elif [ "$1" = "1" ]; then
  ssh root@152.118.148.103 -p 21017
elif [ "$1" = "2" ]; then
  ssh root@152.118.148.103 -p 22017
elif [ "$1" = "3" ]; then
  ssh root@152.118.148.103 -p 23017
elif [ "$1" = "4" ]; then
  ssh root@152.118.148.103 -p 24017
elif [ "$1" = "parent-run" ]; then
  ssh -t root@152.118.148.103 -p 20017 "$2"
elif [ "$1" = "nodes-run" ]; then
  ssh -t root@152.118.148.103 -p 21017 "$2"
  ssh -t root@152.118.148.103 -p 22017 "$2"
  ssh -t root@152.118.148.103 -p 23017 "$2"
  ssh -t root@152.118.148.103 -p 24017 "$2"
elif [ "$1" = "all-run" ]; then
  ssh -t root@152.118.148.103 -p 20017 "$2"
  ssh -t root@152.118.148.103 -p 21017 "$2"
  ssh -t root@152.118.148.103 -p 22017 "$2"
  ssh -t root@152.118.148.103 -p 23017 "$2"
  ssh -t root@152.118.148.103 -p 24017 "$2"
elif [ "$1" = "nodes-run-parallel" ]; then
  ssh -t -n root@152.118.148.103 -p 21017 "$2" &
  ssh -t -n root@152.118.148.103 -p 22017 "$2" &
  ssh -t -n root@152.118.148.103 -p 23017 "$2" &
  ssh -t -n root@152.118.148.103 -p 24017 "$2" &
elif [ "$1" = "nodes-scp" ]; then
  scp -P 21017 "$2" root@152.118.148.103:"$3"
  scp -P 22017 "$2" root@152.118.148.103:"$3"
  scp -P 23017 "$2" root@152.118.148.103:"$3"
  scp -P 24017 "$2" root@152.118.148.103:"$3"
elif [ "$1" = "nodes-git-pull" ]; then
  ssh -t root@152.118.148.103 -p 21017 "cd $2; git pull;"
  ssh -t root@152.118.148.103 -p 22017 "cd $2; git pull;"
  ssh -t root@152.118.148.103 -p 23017 "cd $2; git pull;"
  ssh -t root@152.118.148.103 -p 24017 "cd $2; git pull;"
elif [ "$1" = "all-reboot" ]; then
  ssh -t root@152.118.148.103 -p 20017 '~/bootscript.sh;'
  ssh -t root@152.118.148.103 -p 21017 '~/bootscript.sh;'
  ssh -t root@152.118.148.103 -p 22017 '~/bootscript.sh;'
  ssh -t root@152.118.148.103 -p 23017 '~/bootscript.sh;'
  ssh -t root@152.118.148.103 -p 24017 '~/bootscript.sh;'
elif [ "$1" = "nodes-reboot" ]; then
  ssh -t root@152.118.148.103 -p 21017 '~/bootscript.sh;'
  ssh -t root@152.118.148.103 -p 22017 '~/bootscript.sh;'
  ssh -t root@152.118.148.103 -p 23017 '~/bootscript.sh;'
  ssh -t root@152.118.148.103 -p 24017 '~/bootscript.sh;'
fi