const express = require('express');
const createHmac = require('create-hmac');
const router = express.Router();
const helpers = require('../helpers');
const config = require('../config');

const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

/* GET home page. */
router.post('/login', function(req, res, next) {
  const { username, password } = req.body;
  const requestSignature = req.header('X-Service-Signature');

  db.each(`SELECT username, password, secret FROM users WHERE username='${username}' AND password='${password}'`,
    (err, row) => { // row callback
      const { secret } = row;

      const signature =
        createHmac('SHA256', secret)
        .update(JSON.stringify(req.body)).digest('base64');

      if (requestSignature === signature) {
        res.send({
          status: 'ok',
          token: helpers.getToken(username, password),
        });
      } else {
        res.send({
          status: 'failed',
          token: helpers.getToken(username, password),
        });
      }
    },
    (err, num_rows) => { // completion callback
      if (err) {
        res.send({
          status: 'failed',
          message: 'Database error'
        });
      }
      else if (num_rows == 0) {
        res.send({
          status: 'failed',
          message: 'Incorrect password or user is not registered'
        });
      }
    }
  );
});

router.post('/register', function(req, res, next) {
  const { username, password, displayName } = req.body;
  const d = new Date();
  const secret = helpers.getSecret(username, password);

  db.run(
    "INSERT INTO users VALUES ($username, $password, $displayname, $timestamp, $secret)", {
      $username: username,
      $password: password,
      $displayname: displayName,
      $timestamp: d.toISOString(),
      $secret: secret
    },
    (err) => {
      if (err) {
        if (err.code === 'SQLITE_CONSTRAINT') {
          res.send({
            message: 'User already exists',
          });
        } else {
          res.send({
            message: err,
          });
        }
      } else {
        const response = {
          status: 'ok',
          userId: this.lastID,
          displayName: displayName,
          secret: secret,
        }
        
        res.send(response);
      }
    }
  );
});

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
